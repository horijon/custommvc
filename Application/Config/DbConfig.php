<?php
/**
 * Created by PhpStorm.
 * User: Kshitij
 * Date: 7/14/2016
 * Time: 3:25 PM
 */

namespace Application\Config;

use PDO;

class DbConfig {
    private $host;
    private $username;
    private $password;
    private $db;

    public function GetDbConnection()
    {
        $this->host = "localhost";
        $this->username = "root";
        $this->password = "";
        $this->db = "ecm";

        $conn = new PDO("mysql:host=$this->host;dbname=$this->db", $this->username, $this->password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }
} 