<?php
/**
 * Created by PhpStorm.
 * User: Kshitij
 * Date: 7/14/2016
 * Time: 3:12 PM
 */

namespace System;


class BaseController {

    public function LoadView($path,$data = "")
    {
        if(!empty($data)) {
            extract($data);
        }

        include_once "Application/Admin/Views/".$path.".php";
    }

} 