<?php
/**
 * Created by PhpStorm.
 * User: Kshitij
 * Date: 7/14/2016
 * Time: 3:32 PM
 */

namespace Application\Admin\Repository;


use System\Repo;

class TestRepository extends Repo
{

    function __construct(){
        $this->table = "country";
        $this->model = "Test";
    }

    public function GetEmployee()
    {
        $data = $this->Get();
        return $data;
    }

} 