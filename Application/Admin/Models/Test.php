<?php
namespace Application\Admin\Models;

class Test {

    /* Member variables */
    var $ID;
    var $Country;
    var $Status;

    public function __construct(){
//        $this->ID = 0;
//        $this->Country = "Nepal";
//        $this->Status = "Great";
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->Country;
    }

    /**
     * @param string $Country
     */
    public function setCountry($Country)
    {
        $this->Country = $Country;
    }

    /**
     * @return int
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * @param int $ID
     */
    public function setID($ID)
    {
        $this->ID = $ID;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->Status;
    }

    /**
     * @param string $Status
     */
    public function setStatus($Status)
    {
        $this->Status = $Status;
    }


    /* Member functions */

}
?>