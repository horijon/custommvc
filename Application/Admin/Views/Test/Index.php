<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript">

        var arrayFromPHP = <?php echo $emp; ?>;
        var perPage = 10;
        var pages = Math.ceil(arrayFromPHP.length / perPage);
        var page = 1;

        $(document).ready(function () {
            $('#previous').attr("disabled", true);
            for (i = 1; i <= pages; i++) {
                $('#divNext').append('<button id="pageNumber' + i + '" type="button" onclick="pageNumber(' + i + ')">' + i + '</button>');
            }
            display(page);
        });
        function display(page) {
            for (i = (page - 1) * perPage; i < perPage * page; ++i) {
                if (i == arrayFromPHP.length) {
                    break;
                }
                $('#myTable').append("<tr><td>" + arrayFromPHP[i]['ID'] + "</td><td>" + arrayFromPHP[i]['Country'] + "</td><td>" + arrayFromPHP[i]['Status'] + "</td></tr>");
            }
            $('button').css('color', '#0000EE');

            if (page < pages && $('#next').is(":disabled")) {
                $('#next').removeAttr("disabled");
            } else if (page > 1 && $('#previous').is(":disabled")) {
                $('#previous').removeAttr("disabled");
            }
            if (page == 1) {
                $('#previous').attr("disabled", true).css('color', '');
            } else if (pages == page) {
                $('#next').attr("disabled", true).css('color', '');
            }

        }

        function pageNumber(pageNumber) {
            $('#myTable').empty();
            page = pageNumber;
            display(page);
        }

    </script>
</head>

<body>
<table border="1" id="myTable" style="background: #eee; border-color: white">
</table>
<div id="divNext" style="float: left">
    <button id="next" type="button" onclick="pageNumber(++page)">Next</button>
</div>
<div style="margin-left: 10px">
    <button id="previous" type="button" onclick="pageNumber(--page)">Previous</button>
</div>
</body>
</html>