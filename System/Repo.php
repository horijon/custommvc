<?php
/**
 * Created by PhpStorm.
 * User: Kshitij
 * Date: 7/14/2016
 * Time: 3:33 PM
 */

namespace System;


use Application\Config\DbConfig;
use PDO;

class Repo extends DbConfig
{

    public $table;

    public $model;

    public function Get()
    {

        $sql = "SELECT * FROM ".$this->table;
        $conn= $this->GetDbConnection();
        $sqlQuery = $conn->prepare($sql);
        $sqlQuery->execute();
        $data = $sqlQuery->fetchAll(PDO::FETCH_ASSOC);
//        $row = $sqlQuery->rowCount();
//        $all = array($data,$row);
//        return $all;
        return $data;

    }

    public function GetByID($ID)
    {

        $sql = "SELECT * FROM " . $this->table . " Where ID = :ID";
        $sqlQuery = $this->GetDbConnection()->prepare($sql);
        $sqlQuery->bindParam(":ID", $ID);
        $sqlQuery->execute();
        $data = $sqlQuery->fetch(PDO::FETCH_ASSOC);
        $modelData = $this->MapModel($data);
        return $modelData;

    }

    public function MapModel($data)
    {
        $m = "Application\\Admin\\Models\\".$this->model;
        $model = new $m();
        $modelAttribute = get_object_vars($model);

        foreach($modelAttribute as $attrKey=> $attrVal) {
            foreach($data as $dataKey => $dataVal) {
                if($attrKey == $dataKey) {
                    $model->$attrKey = $dataVal;
                }
            }
        }

        return $model;
    }
} 