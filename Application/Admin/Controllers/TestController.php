<?php

namespace Application\Admin\Controllers;

//use Application\Admin\Models\Test;
use Application\Admin\Models\Test;
use Application\Admin\Repository\TestRepository;
use System\BaseController;

class TestController extends BaseController
{

    public function Index()
    {
        $test = new Test();
        $testRepo = new TestRepository();
        $emp = $testRepo->GetEmployee();

        $test = $testRepo->GetByID(6);

        $param["emp"] = json_encode($emp);
        $param["testModel"] = $test;

        $this->LoadView("Test/Index", $param);
    }

    public function json(){
        $arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
        return json_encode($arr);
    }

}


